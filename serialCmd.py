#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Module is a CLI utility to interact with a serial device
For support: igor.skh@gmail.com (Igor Kim)
03/2018, Leipzig
"""

import argparse, readline, logging
import serialDevice

__author__ = "Igor Kim"
__credits__ = ["Igor Kim"]
__maintainer__ = "Igor Kim"
__email__ = "igor.skh@gmail.com"
__status__ = "Development"
__date__ = "03/2018"
__license__ = ""

class SerialInterface(serialDevice.SerialInterface):
    def commandLoop(self):
        cmd = ""
        while cmd != "exit":
            cmd = str(input())
            if cmd == "exit":
                break
            self.send(cmd)

    def proceedScript(self, filepath):
        with open(filepath) as f:
            for line in f:
                self.send(line.strip())

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--device", default="/dev/ttyUSB0", help="tty device name")
    parser.add_argument("-b","--baud", default=9600, help="baudrate")
    parser.add_argument("-s","--script", help="path to the script")
    parser.add_argument("-t","--timeout", help="timeout type")

    logger = logging.getLogger(__package__)
    logger.setLevel(logging.INFO)    
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)    

    args = parser.parse_args()

    dev = SerialInterface(args.device, args.baud, args.timeout)
    dev.initDevice()

    if dev.device is None:
        logger.fatal("unable to connect to %s"%dev.port)
        exit()

    if dev.device.isOpen():   
        dev.startReadThread()
        if args.script is None:
            dev.commandLoop()
        else:
            dev.proceedScript(args.script)
        dev.device.close()
    else:
        logger.fatal("cannot open serial port %s"%dev.port)

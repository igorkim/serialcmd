#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Module sample to interact with a serial device
For support: igor.skh@gmail.com (Igor Kim)
03/2018, Leipzig
"""

import serial, time, string
import threading, queue

__author__ = "Igor Kim"
__credits__ = ["Igor Kim"]
__maintainer__ = "Igor Kim"
__email__ = "igor.skh@gmail.com"
__status__ = "Development"
__date__ = "03/2018"
__license__ = ""

class SerialInterface():
    device = None

    OK_MSG = 0
    ERR_MSG = 1
    EOT_MSG = 2
    ASYNC_MSG = 3
    PRINT_MSG = 4
    APPEND_MSG = 5

    def __init__(self, port="/dev/ttyUSB0", baudrate=9600, timeout=None):
        """Init
        
        Keyword Arguments:
            port {str} -- path to a serial device (default: {"/dev/ttyUSB0"})
            baudrate {int} -- baud rate (default: {9600})
            timeout {object} -- None if no timeout otherwise int (default: {None})
        """
        self.readQueue = queue.Queue()
        self.asyncQueue = queue.Queue()
        self.recv = False
        self.outBuff = ""
        #
        self.port = port
        self.baudrate = baudrate
        self.timeout = timeout
        self.whileSleepPeriod = 0.1

    def parseOutput(self, buffer):
        """Determine the type of the message
        
        Arguments:
            buffer {string} -- message to categorize
        
        Returns:
            string -- message type
        """
        tmp = buffer
        status = []
        if tmp.find("OK")>=0:
            status.append(self.OK_MSG)
        elif tmp.find("ERROR")>=0:
            status.append(self.ERR_MSG)
        if tmp.find("+")==0:
            status.append(self.ASYNC_MSG)
        if tmp.find("\r\n")>=0:
            status.append(self.EOT_MSG)
        if tmp.find("\n")<0:
            status.append(self.APPEND_MSG)
        if len(status)==0:
            status.append(self.PRINT_MSG)
        return status        

    def initDevice(self):
        """Initialize serial device
        
        Returns:
            object -- Serial object if successfull, None otherwise
        """
        #possible timeout values:
        #    1. None: wait forever, block call
        #    2. 0: non-blocking mode, return immediately
        #    3. x, x is bigger than 0, float allowed, timeout block call
        ser = serial.Serial()
        ser.port = self.port
        ser.baudrate = int(self.baudrate)
        ser.bytesize = serial.EIGHTBITS #number of bits per bytes
        ser.parity = serial.PARITY_NONE #set parity check: no parity
        ser.stopbits = serial.STOPBITS_ONE #number of stop bits
        ser.timeout = int(self.timeout) if self.timeout else None
        ser.dtr = 1
        ser.xonxoff = False     #disable software flow control
        ser.rtscts = False     #disable hardware (RTS/CTS) flow control
        ser.dsrdtr = False       #disable hardware (DSR/DTR) flow control
        ser.writeTimeout = 2     #timeout for write
        self.device = ser
        try:
            ser.open()
            return self.device
        except Exception:
            self.device = None
        return self.device
    
    def readLinesThread(self):
        """Thread to add messages to the queue separated by lines
        """
        while True:
            if self.device.inWaiting() > 0:
                response = self.device.read(self.device.inWaiting()).lstrip()
                if response.find(b"\n") > 0:
                    for l in response.splitlines(True):
                        if len(l) > 0:
                            self.readQueue.put(l)
                else:
                    self.readQueue.put(response)
            time.sleep(self.whileSleepPeriod)

    def printThread(self):
        """Thread to print message from the readQueue
        """
        while True:
            if self.readQueue.empty():
                time.sleep(self.whileSleepPeriod)
                continue
            out = self.readQueue.get().decode("unicode_escape")
            filter(lambda x: x in set(string.printable), out)
            msgStatus = self.parseOutput(out)
            out = out.lstrip()
            self.outBuff += out
            if not self.APPEND_MSG in msgStatus:
                self.outBuff = self.outBuff.strip()
                if self.OK_MSG in msgStatus or self.ERR_MSG in msgStatus:
                    print("Status: ", self.outBuff)
                    self.outBuff = ""
                if self.ASYNC_MSG in msgStatus:
                    self.asyncQueue.put(self.outBuff)
                if self.PRINT_MSG in msgStatus:
                    print(self.outBuff)
                    self.outBuff = ""
                if self.EOT_MSG in msgStatus:
                    if self.outBuff != "":
                        print(self.outBuff)
                        self.outBuff = ""    
                    self.recv = False
            time.sleep(self.whileSleepPeriod)

    def startReadThread(self, initPrint=True):
        """Start threads to read from serial and print to stdout
        
        Keyword Arguments:
            print {bool} -- start print thread (default: {True})
        """
        readThread = threading.Thread(target=self.readLinesThread)
        readThread.daemon = True
        readThread.start()
        if initPrint:
            printThread = threading.Thread(target=self.printThread)
            printThread.daemon = True
            printThread.start()           

    def send(self, cmd, timeout=10, silent=False):
        """Send data to serial device
        
        Arguments:
            cmd {string} -- data to send
        
        Keyword Arguments:
            timeout {int} -- timeout to send (default: {10})
            silent {bool} -- echo the command if true (default: {False})
        
        Returns:
            int -- status code
        """
        try:
            cmd = (cmd+"\r\n").encode('ascii') 
            #flush input buffer, discarding all its contents
            self.device.flushInput()
            #flush output buffer, aborting current output
            self.device.flushOutput()

            self.device.write(cmd)
            if not silent:
                print("request: %s"%cmd)
            self.recv = True
            for _ in range(timeout):
                time.sleep(1)
                if not self.recv:
                    return 0
            if self.recv:
                if not silent:
                    print(self.outBuff)
                self.outBuff = ""
                self.recv = False
                if not silent:
                    print("error: recv timeout")
                return 1
        except Exception as e1:
            if not silent:
                print("error: communicating:%s\n"%str(e1))
            return 2